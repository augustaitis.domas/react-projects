import './App.css';
import Profile from './Components/Profile';
import Login from './Components/Login';
import ChangeColour from './Components/ChangeColour';

function App() {
  return (
    <div className="App">
      <Profile />
      <Login />
      <ChangeColour />
    </div>
  );
}

export default App;
